#!/usr/bin/python

# -- Proyecto creado por Sergio Marín Gutiérrez.
import socket
import random

PORT = 1243
response_url = "HTTP/1.1 307 Temporary Redirect \r\n" \
               + "Location: {}\r\n" \
               + "\r\n\r\n"


urls = ['https://www.urjc.es/', 'https://labs.eif.urjc.es/', 'https://gsyc.urjc.es//', 'https://www.upm.es/',
        'https://www.coit.es/']

mySocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
mySocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
mySocket.bind(('', PORT))
mySocket.listen(5)

random.seed()

try:
    while True:
        print("Waiting for connections...")
        (recvSocket, address) = mySocket.accept()
        print("HTTP request received:")
        print(recvSocket.recv(2048))
        number = random.randint(0, len(urls)-1)
        recvSocket.send(response_url.format(urls[number]).encode('utf-8'))
        print(urls[number])
        recvSocket.close()

except KeyboardInterrupt:
    print('An error has ocurred, closing...')
    mySocket.close()
